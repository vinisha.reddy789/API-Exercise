# Solution

## To run the api in local docker env

```bash
$ docker-compose run app-build
$ docker-compose up -d app
```

## To run the api in minikube

```bash
$ docker login -u <dockerhub_username> -p <dockerhub_password>
$ docker build -t <dockerhub_username>/people-mysql:latest -f Dockerfile-db .
$ docker push <dockerhub_username>/people-mysql:latest
$ docker build -t <dockerhub_username>/people-service:latest -f Dockerfile-app .
$ docker push <dockerhub_username>/people-service:latest
$ minikube start
$ kubectl apply -f manifest.yaml
$ minikube tunnel
```