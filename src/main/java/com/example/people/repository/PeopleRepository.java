package com.example.people.repository;

import com.example.people.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface PeopleRepository extends JpaRepository<Person, String> {

    Optional<Person> findByUuid(String uuid);
}
