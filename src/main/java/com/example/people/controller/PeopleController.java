package com.example.people.controller;

import com.example.people.exception.ResourceNotFoundException;
import com.example.people.model.Person;
import com.example.people.repository.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class PeopleController {

    @Autowired
    PeopleRepository peopleRepository;

    @GetMapping("/people")
    public List<Person> getAllPeople() {
        return peopleRepository.findAll();
    }

    @PostMapping("/people")
    public Person createPeople(@Valid @RequestBody Person person) {
        return peopleRepository.save(person);
    }

    @GetMapping("/people/{id}")
    public Person getPersonById(@PathVariable(value = "id") String personId) {
        System.out.println(personId);

        return peopleRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person", "id", personId));
    }

    @PutMapping("/people/{id}")
    public Person updatePerson(@PathVariable(value = "id") String personId,
                                           @Valid @RequestBody Person person) {

        Person personFromDb = peopleRepository.findByUuid(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person", "id", personId));

        personFromDb.setAge(person.getAge());
        personFromDb.setFare(person.getFare());
        personFromDb.setName(person.getName());
        personFromDb.setSex(person.getSex());
        personFromDb.setParentsOrChildrenAboard(person.getParentsOrChildrenAboard());
        personFromDb.setSiblingsOrSpousesAboard(person.getSiblingsOrSpousesAboard());
        personFromDb.setSurvived(person.getSurvived());

        Person updatedPerson = peopleRepository.save(personFromDb);
        return updatedPerson;
    }

    @DeleteMapping("/people/{id}")
    public ResponseEntity<?> deletePerson(@PathVariable(value = "id") String personId) {
        System.out.println(personId);
        Person person = peopleRepository.findByUuid(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person", "id", personId));

        peopleRepository.delete(person);

        return ResponseEntity.ok().build();
    }
}
