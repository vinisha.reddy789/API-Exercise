package com.example.people.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "person")
@EntityListeners(AuditingEntityListener.class)
public class Person {

//    @Id
//    @GeneratedValue
//    @Type(type="org.hibernate.type.UUIDCharType")
//    private UUID uuid;

    @Id
    @Column(name = "uuid", unique = true, nullable = false, length = 36)
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "org.hibernate.id.UUIDGenerator")
    private String uuid;

    private String name;

    private String sex;

    private Boolean survived;

    private Integer passengerClass;

    private Integer age;

    private Double fare;

    private Integer siblingsOrSpousesAboard;

    private Integer parentsOrChildrenAboard;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Boolean getSurvived() {
        return survived;
    }

    public void setSurvived(Boolean survived) {
        this.survived = survived;
    }

    public Integer getPassengerClass() {
        return passengerClass;
    }

    public void setPassengerClass(Integer passengerClass) {
        this.passengerClass = passengerClass;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public Integer getSiblingsOrSpousesAboard() {
        return siblingsOrSpousesAboard;
    }

    public void setSiblingsOrSpousesAboard(Integer siblingsOrSpousesAboard) {
        this.siblingsOrSpousesAboard = siblingsOrSpousesAboard;
    }

    public Integer getParentsOrChildrenAboard() {
        return parentsOrChildrenAboard;
    }

    public void setParentsOrChildrenAboard(Integer parentsOrChildrenAboard) {
        this.parentsOrChildrenAboard = parentsOrChildrenAboard;
    }
}
